Firefox
=========

Role that installs Firefox web browser

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Firefox on targeted machine:

    - hosts: servers
      roles:
         - firefox

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
